### Kubectl commands

##### apply manifests
    kubectl apply -f nginx-deployment.yaml
    kubectl apply -f nginx-service.yaml

##### labels
    kubectl get all
    kubectl get svc
    kubectl describe svc {svc-name}
    kubectl edit svc {svc-name}
    kubectl get svc {svc-name} -o yaml
    kubectl get ep

    kubectl get svc --show-labels
    kubectl get svc -l app=nginx 

    kubectl get pod --show-labels
    kubectl get pod -l app=nginx
    kubectl logs -l app=nginx

    kubectl get pod -n kube-system --show-labels
    kubectl logs -n kube-system -l="name=weave-net" -c weave

    kubectl get node —show-labels


##### scaling deployments
    kubectl scale --help
    kubectl scale deployment {depl-name} --replicas=4
    kubectl scale deployment {depl-name} --replicas=3

    kubectl scale deployment {depl-name} --replicas 5 --record
    kubectl rollout history deployment {depl-name}


##### create pods
    kubectl run test-nginx-service --image=busybox
    kubectl exec -it {pod-name} -- bash
    cat /etc/resolv.conf
    
#### To see kubeadm init default parameters
    kubeadm config print init-defaults

### create deployment by kubectl command, and expor to yaml file
    kubectl create deployment my-deployment --image=nginx:1.20 --port=80 --replicas=3 --dry-run=client  -o yaml > my-deployment.yaml

#### create service by kubectl command
    kubectl create service clusterip my-service --tcp=8080:80 --dry-run=client -o yaml > my-service.yaml

#### create pod
    kubectl run my-pod --image=nginx:1.20 --labels='app=nginx, env=prod' --dry-run=client -o yaml > my-pod.yaml

    
