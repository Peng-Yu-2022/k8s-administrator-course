#### If you get an error on creating ingress component related to "nginx-controller-admission" webhook, than manually delete the ValidationWebhook and try again. To delete the ValidationWebhook:
    kubectl get ValidatingWebhookConfiguration 
    kubectl delete ValidatingWebhookConfiguration {name}

[Link to a more detailed description of the issue](https://pet2cattle.com/2021/02/service-ingress-nginx-controller-admission-not-found)



##### install helm
    curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
    sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/    apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install helm

#### list the charts it has 
    helm repo list

#### add chart to the repository
    helm repo add nginx-stable https://helm.nginx.com/stable

#### update repo
    helm repo update

#### install nginx-ingress controller to kubernetes
    helm install nginx-ingress nginx-stbale/nginx-ingress
