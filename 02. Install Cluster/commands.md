### Provision Infrastructure 

##### move private key to .ssh folder and restrict access
    mv ~/Downloads/k8s-node.pem ~/.ssh
    chmod 400 ~/.ssh/k8s-node.pem

##### ssh into ec2 instance with its public ip
    ssh -i ~/.ssh/k8s-node.pem ubuntu@35.180.130.108
#### switch to root user
    sudo -i


### Configure Infrastructure
    sudo swapoff -a

##### set host names of nodes
    sudo vim /etc/hosts

##### get priavate ips of each node and add this to each server 
    45.14.48.178 master
    45.14.48.179 worker1
    45.14.48.180 worker2

##### we can now use these names instead of typing the IPs, when nodes talk to each other. After that, assign a hostname to each of these servers.

##### on master server
    sudo hostnamectl set-hostname master 

##### on worker1 server
    sudo hostnamectl set-hostname worker1 

##### on worker2 server
    sudo hostnamectl set-hostname worker2

#### Install and configure prerequisites 
    cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
    overlay
    br_netfilter
    EOF

    sudo modprobe overlay
    sudo modprobe br_netfilter

    # sysctl params required by setup, params persist across reboots
    cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
    net.bridge.bridge-nf-call-iptables  = 1
    net.bridge.bridge-nf-call-ip6tables = 1
    net.ipv4.ip_forward                 = 1
    EOF

    # Apply sysctl params without reboot
    sudo sysctl --system

#### install containerd
    sudo apt update
    sudo apt install containerd

#### create config file for containerd option 1
    sudo mkdir /etc/containerd
    containerd config default | sudo Tee /etc/containerd/config.toml

#### create config file for containerd option 2
    sudo -i
    mkdir /etc/containerd
    containerd config default > /etc/containerd/config.toml

#### Check containerd status
    sudo systemctl status containerd

#### restart containerd
    sudo systemctl restart containerd

#### install containerd in worknode 1
    vim install-containerd.sh
    chmod +x install-containerd.sh
    ./install-containerd.sh

#### install containerd in worknode 2
    vim install-containerd.sh
    chmod +x install-containerd.sh
    ./install-containerd.sh


#### install kubeadm, kubectl, and kubelet
    sudo apt-get update
    sudo apt-get install -y apt-transport-https ca-certificates curl

    curl -fsSL https://dl.k8s.io/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-archive-keyring.gpg
    echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

    sudo apt-get update
    sudo apt-get install -y kubelet=1.23.1-00 kubeadm=1.23.1-00 kubectl=1.23.1-00
    sudo apt-mark hold kubelet kubeadm kubectl

#### to check versions of kubeadm
    apt-cache madison kubeadm

#### check kubelet status
    service kubelet status

### Initialize K8s cluster
    sudo kubeadm init

### kubectl command option 1
    sudo kubectl get node --kubeconfig /etc/kubernetes/admin.conf

### kubectl command option 2
    sudo -i
    export KUBECONFIG=/etc/kubernetes/admin.conf
    kubectl get node

### kubectl command option 3
    mkdir ~/.kube
    sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
    sudo chname $(id -u):$(id -g) ~/.kube/config
    kubectl get node

#### install weave as agent for pod communication in the cluster
    kubectl apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml

#### join the first, second node
#### command in the master node
    kubeadm token create --kubeadm token create --print-join-command
#### command in the node which will be joined
    kubeadm join 172.31.83.0:6443 --token 4j2c9k.0v4gtsbuo8rw9zlm --discovery-token-ca-cert-hash sha256:54cea09673c06a0d2f95eb2b3782dac9de6f3dbcb93aedc8873a13787fb54d54 

#### grab only weave-net pod
    kubectl get pod -A -o wide | grep weave-net

### Check kubelet process running 
    service kubelet status
    systemctl status kubelet

### Check extended logs of kubelet service
    journalctl -u kubelet

### Access cluster as admin
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config

### Kubectl commands

##### get node information
    kubectl get node

##### get pods in kube-system namespace
    kubectl get pod -n kube-system

##### get pods from all namespaces
    kubectl get pod -A

##### get wide output
    kubectl get pod -n kube-system -o wide


### Install pod network plugin

##### download and install the manifest
    kubectl apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml

[Link to the Weave-net installation guide](https://www.weave.works/docs/net/latest/kubernetes/kube-addon/#-installation)    

##### check weave net status
    kubectl exec -n kube-system weave-net-1jkl6 -c weave -- /home/weave/weave --local status

### Join worker nodes

##### create and execute script
    vim install-containerd.sh
    chmod u+x install-containerd.sh
    ./install-containerd.sh

##### on master
    kubeadm token create --help
    kubeadm token create --print-join-command

##### copy the output command and execute on worker node as ROOT
    sudo kubeadm join 172.31.43.99:6443 --token 9bds1l.3g9ypte9gf69b5ft --discovery-token-ca-cert-hash sha256:xxxx

##### start a test pod
    kubectl run test --image=nginx


